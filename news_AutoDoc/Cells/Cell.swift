//
//  Cell.swift
//  news_AutoDoc
//
//  Created by Григорий Ищенко on 22.06.2022.
//

import UIKit

class Cell: UICollectionViewCell {

    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var chevronImageView: UIImageView!
    
    static let reuseIdentifier = "Cell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 10.0
        layer.masksToBounds = true
    }

}

