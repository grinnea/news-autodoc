//
//  Details.swift
//  news_AutoDoc
//
//  Created by Григорий Ищенко on 22.06.2022.
//

import UIKit
import WebKit

class Details: UIViewController {
    
    @IBOutlet weak var webView: WKWebView!
    
    var newsFeullUrl: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showNews()
    }
    
    func showNews() {
        let urlString = newsFeullUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        let myURL = URL(string:urlString)
              let myRequest = URLRequest(url: myURL!)
              webView.load(myRequest)
    }
}
