//
//  Model.swift
//  news_AutoDoc
//
//  Created by Григорий Ищенко on 22.06.2022.
//

import Foundation

struct NewsDataList: Hashable, Codable {
    let news: [NewsDetail]?
    let totalCount: Int?
}

struct NewsDetail: Hashable, Identifiable, Codable {
    let id: Int?
    let title: String?
    let description: String?
    let publishedDate: String?
    let url: String?
    let fullUrl: String?
    let titleImageUrl: String?
    let categoryType: String?
}

