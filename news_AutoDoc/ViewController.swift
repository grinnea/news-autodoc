//
//  ViewController.swift
//  news_AutoDoc
//
//  Created by Григорий Ищенко on 22.06.2022.
//

import UIKit
import Combine

class ViewController: UIViewController, UICollectionViewDelegate {

    enum Section {
        case main
    }
    
    var dataSource: UICollectionViewDiffableDataSource<Section, NewsDetail>! = nil
    var collectionView: UICollectionView! = nil
    
    private let viewModel = ViewModel()
    private var cancellable = Set<AnyCancellable>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Новости"
        configureHierarchy()
        configureDataSource()
        applySnapshot(animatingDifferences: true)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshData), name: NSNotification.Name("refreshData"), object: nil)
    }
    
    @objc func refreshData(notification: NSNotification) {
        applySnapshot()
    }
    
    private func createLayout() -> UICollectionViewLayout {
        let itemSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1.0),
            heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        item.contentInsets = .init(top: 5, leading: 10, bottom: 5, trailing: 10)
        
        let groupSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1.0),
            heightDimension: .absolute(80))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
        
        
        let section = NSCollectionLayoutSection(group: group)
        
        let layout = UICollectionViewCompositionalLayout(section: section)
        return layout
    }
    
    private func configureHierarchy() {
        collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: createLayout())
        collectionView.delegate = self
        collectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        collectionView.backgroundColor = .systemBackground
        let nib = UINib(nibName: Cell.reuseIdentifier, bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: Cell.reuseIdentifier)
        view.addSubview(collectionView)
    }
    
    private func configureDataSource() {
        dataSource = UICollectionViewDiffableDataSource<Section, NewsDetail>(collectionView: collectionView) {
            (collectionView: UICollectionView, indexPath: IndexPath, identifier: NewsDetail) -> UICollectionViewCell? in

            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Cell.reuseIdentifier, for: indexPath) as? Cell else { fatalError("Cannot create the cell") }

            cell.titleLabel.text = self.viewModel.newsList.news?[indexPath.row].title
            cell.iconImageView.load(url: (self.viewModel.newsList.news?[indexPath.row].titleImageUrl) ?? "")

            return cell
        }
    }
    
    func applySnapshot(animatingDifferences: Bool = true) {
        var snapshot = NSDiffableDataSourceSnapshot<ViewController.Section, NewsDetail>()
        snapshot.appendSections([Section.main])
        snapshot.appendItems(viewModel.newsList.news ?? [])
        dataSource.apply(snapshot, animatingDifferences: animatingDifferences)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard!.instantiateViewController(identifier: "Details") as! Details
        vc.newsFeullUrl = self.viewModel.newsList.news?[indexPath.row].fullUrl
        if let navController = navigationController {
            navController.pushViewController(vc, animated: true)
        }
    }
}

extension UIImageView {
    func load(url: String) {
        guard let urlLink = URL(string: url) else { return }
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: urlLink) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
