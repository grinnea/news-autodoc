//
//  NewsAPI.swift
//  news_AutoDoc
//
//  Created by Григорий Ищенко on 22.06.2022.
//

import Foundation
import Combine

class NewsAPI {
    static let shared = NewsAPI()
    func fetch() -> AnyPublisher<NewsDataList, Error> {
        guard let url = URL(string: "https://webapi.autodoc.ru/api/news/1/15") else {
                fatalError("Invalid URL")
            }
        return URLSession.shared
            .dataTaskPublisher(for: url)
            .map(\.data)
            .decode(type: NewsDataList.self, decoder: JSONDecoder())
            .mapError { (error) -> Error in
                print(error)
                return error
            }
            .eraseToAnyPublisher()
    }
}

