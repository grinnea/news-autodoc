//
//  ViewModel.swift
//  news_AutoDoc
//
//  Created by Григорий Ищенко on 22.06.2022.
//

import Combine
import Foundation

class ViewModel: ObservableObject {
    @Published var newsList: NewsDataList = NewsDataList(news: [], totalCount: 0)
    var cancellation: AnyCancellable?
    let service = NewsAPI()

    init() {
        fetchNews()
    }
    
    func fetchNews() {
        cancellation = service.fetch()
            .mapError({ (error) -> Error in
                print(error)
                return error
            })
            .sink(receiveCompletion: { _ in },
                  receiveValue: { [weak self] (newsList) in
                self?.newsList = newsList
                NotificationCenter.default.post(name: NSNotification.Name("refreshData"), object: nil)
            })
    }
}
